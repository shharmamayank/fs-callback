let fs = require('fs');
let path = require('path');

function readFile() {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(__dirname, 'lipsum.txt',), (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data)
            }
        })
    })
}
function createFile(maindata) {

    return new Promise((resolve, reject) => {
        let UpperCasedata = (maindata.toString().toUpperCase());
        fs.writeFile(path.join(__dirname, `UpperCaseFile.txt`), `${UpperCasedata}`, (err, data) => {
            if (err) {
                console.log(err);
            } else {
                fs.writeFile(path.join(__dirname, `FileNames.txt`), `UpperCaseFile.txt`, (err, data) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("File Converted to UpperCase and written to OtherFile");

                        resolve(maindata)

                    }

                })
            }
        })
    })
}
function getLowerCases(data) {
    return new Promise((resolve, reject) => {
        let lowerCaseData = data.toString().toLowerCase().split('. ').toString()

        fs.writeFile(path.join(__dirname, `lowerCaseFile.txt`), `${lowerCaseData}`, (err, data) => {
            if (err) {
                console.log(err);
            } else {
                fs.appendFile(path.join(__dirname, `FileNames.txt`), ` lowerCaseFile.txt`, (err, data) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("File Converted to lowerCase and written to OtherFile");
                        resolve(data)
                    }

                })
            }
        })

    })
}

function ReadAndAppend(params) {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(__dirname, 'lowerCaseFile.txt'), (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let lowerArray = (data.toString().split(' '))

                lowerArray.sort((first, second) => {
                    return first.localeCompare(second)
                })
                fs.readFile(path.join(__dirname, 'UpperCaseFile.txt'), function (err, data) {
                    if (err) {
                        console.log(err);
                    } else {

                        let upperArray = (data.toString().split(' '));

                        upperArray.sort((first, second) => {
                            return first.localeCompare(second);
                        });
                        fs.appendFile(path.join(__dirname, 'sorted.txt'), JSON.stringify(`${upperArray}`), (err, data) => {
                            if (err) {
                                console.log(err);
                            } else {
                                console.log('files written to sorted.txt');
                                fs.appendFile(path.join(__dirname, 'FileNames.txt'), ' sorted.txt', (err, data) => {
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        console.log('sorted.txt written to FileNames.txt');
                                        resolve()
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    })




}

function readAndDelete(data) {
    console.log("this is delete");
    fs.readFile(path.join(__dirname, 'FileNames.txt'), (err, data) => {
        if (err) {
            console.log(err);
        } else {
            fileNameArray = data.toString().split(' ')
            for (let index = 0; index < fileNameArray.length; index++) {
                fs.unlink(path.join(__dirname, `${fileNameArray[index]}`), (err, data) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(`${fileNameArray[index]}` + 'file deleted');
                    }
                })
            }
        }
    })


}


module.exports.read = readFile
module.exports.write = createFile
module.exports.lower = getLowerCases
module.exports.append = ReadAndAppend
module.exports.Delete = readAndDelete