let fs = require('fs');
let path = require("path")
function createAndDelete(dirName) {
    let folderName = dirName;
    let nameArray = Array(5).fill(0).map((n, index) => {
        return index
    })
    return new Promise((resolve, reject) => {
        fs.mkdir(path.join(__dirname, `${folderName}`), (err, data) => {
            if (err) {
                reject((err))
            } else {
                resolve(nameArray)
            }
        })
    })
}
function createFile(nameArray) {
    return new Promise((resolve, reject) => {
        for (let index = 0; index < nameArray.length; index++) {
            fs.writeFile(path.join(__dirname, `${folderName}`, `${nameArray[index]}.JSON`), JSON.stringify({ 1: 'Dummy-Data' }), (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    console.log((`${nameArray[index]}.JSON` + "Files-Created"));
                    resolve(nameArray)
                }
            })
        }
    })
}
function deleteFile(nameArray) {
    new Promise((resolve, reject) => {
        for (let index = 0; index < nameArray.length; index++) {
            fs.unlink(path.join(__dirname, `${folderName}`, `${nameArray[index]}.JSON`), (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(`${nameArray[index]}.JSON` + "files-Deleted");
                }
            })
        }
    })
}


module.exports.dir = createAndDelete;
module.exports.file = createFile
module.exports.delete = deleteFile